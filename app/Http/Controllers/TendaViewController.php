<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;
use App\category;

class TendaViewController extends Controller
{
    public function postItem(Request $request)
    {
        $explode = explode(',', $request->image_product);
        $decoded = base64_decode($explode[1]);

        if(str_contains($explode[0], 'jpeg'))
            $extension = 'jpg';
        elseif(str_contains($explode[0], 'gif'))
            $extension = 'gif';
        else
            $extension = 'png';

        $filename = str_random() .'.'. $extension;
        $path = public_path(). '/img/items/' . $filename;
        file_put_contents($path, $decoded);

        $product = new product;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->content = $request->content;
        $product->image_product = $filename;
        $product->save();
    }

    public function product()
    {
        $product = product::all();
        return response()->json(['product' => $product], 200);
    }

    public function category()
    {
        $category = category::all();
        return response()->json(['category' => $category], 200);
    }

}
