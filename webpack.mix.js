let mix = require('laravel-mix');
const browsync = require('browser-sync-webpack-plugin');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.webpackConfig({
	plugins: [
		new browsync({
			open: 'external',
			proxy: 'localhost:8000',
      port: '8080',
			files: ['resources/views/**/*.php', 'app/**/*.php', 'routes/**/*.php']
		})
	]
});



mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
