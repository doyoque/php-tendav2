## Laravel and vue

**Latest build: 2018-03-05**

**Note: This project is still in progress, but welcome for any issues encountered**

A pre-build website using Laravel framework.

This repository is developed upon the following tools:

* [CRUDBooster](http://crudbooster.com/) (^v5.4) - CRUD library for admin panel.
* [Vuejs](https://vuejs.org/) (^v2.5.7) - Progressive javascript framework.
* [Vue-router](https://router.vuejs.org/) (^v3.0.1) - One of the vue core libraries.
* [laravel](https://laravel.com) (v5.5.^) - PHP framework.
## Features

* Pre-build vue-router script (routes.js). It's already configured to app.js.
* Preset components vue.
* Preset Admin panel build (access with http://yourdomain/admin/login).
* ... more coming!.

## Server environment

Below the comfiguration are preffered.

* **PHP 7.1+**
* **Apache2**
* **MySQL**

## Setup Guide

1. git clone this repo.
2. Create database.
3. run composer install command.
4. run npm install command.
