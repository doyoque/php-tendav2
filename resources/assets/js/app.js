
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router'; //call vue router lib
import routes from './routes.js'; //import the routes.js
import * as VueGoogleMap from 'vue2-google-maps';

Vue.use(VueRouter);
Vue.use(VueGoogleMap, {
  load: {
    key: 'AIzaSyBGsT65RzN1dlNTGToMUDexKzCnaBmSn0Y',
    libraries: 'places',
    v: '3.26'
  }
});

const router = new VueRouter({
	hashbang: false,
  mode: 'history', //without this the URL will be ugly
	routes
});

import navbar from './components/layouts/NavbarComponent.vue';
import sidebar from './components/layouts/SidebarComponent.vue';
import practice from './components/PracticeComponent.vue';

const app = new Vue({
    el: '#app',
    router,
    components: {
      practicecomponent: practice,
      navbarcomponent: navbar,
      sidebarcomponent: sidebar
    }
});

$(document).ready(function () {
  $("#sidebar").mCustomScrollbar({
    theme: "minimal"
  });

  $('#dismiss, .overlay').on('click', function () {
    $('#sidebar').removeClass('active');
    $('.overlay').fadeOut();
  });

  $('#sidebarCollapse').on('click', function () {
    $('#sidebar').addClass('active');
    $('.overlay').fadeIn();
    $('.collapse.in').toggleClass('in');
    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
  });
});