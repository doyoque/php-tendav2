import index from './components/Index.vue'
import about from './components/About.vue'
import register from './components/Register.vue'
import companies from './components/Companies.vue'
import compEdit from './components/CompaniesEdit.vue'
import compCreate from './components/CompaniesCreate.vue'

const routes = [
  { path: '/', component: index },
  { path: '/about', component: about },
  { path: '/register', component: register },
  { path: '/companies', component: companies },
  { path: '/companies/edit/:id', component: compEdit, name: 'editCompany' },
  { path: '/companies/create', component: compCreate }
];

export default routes;